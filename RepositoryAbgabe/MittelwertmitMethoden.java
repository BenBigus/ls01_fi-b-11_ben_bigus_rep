import java.util.Scanner;
public class MittelwertmitMethoden {

	public static void main(String[] args) {
		
		//Deklaration von Variablen
		double zahl1;
		double zahl2;
		double m;
		Scanner myScanner = new Scanner(System.in);
		
		System.out.println("Dieses Programm berechnet den Mittelwert zweier Zahlen.");
		
		zahl1 = eingabe("Bitte geben Sie die erste Zahl ein: ", myScanner);
		zahl2 = eingabe("Bitte geben Sie die zweite Zahl ein: ", myScanner);
		m = berechnetMittelwert(zahl1, zahl2);
		ausgabe(m);
		
		myScanner.close();

	}
	
	public static double eingabe(String text, Scanner myScanner) {
		
		System.out.println(text);
		double zahl = myScanner.nextDouble();
		return zahl;
	}
	
	public static double berechnetMittelwert(double zahl1, double zahl2) {
		double m = (zahl1 + zahl2)/ 2.0;
		return m;
	}
	
	public static void ausgabe(double m) {
		System.out.println("Mittelwert:" + m);
		
		
	}

}
